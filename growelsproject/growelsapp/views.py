
from django.shortcuts import render,HttpResponse,HttpResponseRedirect,redirect
# Create your views here.
from growelsapp.forms import UserCategoryForm,galleryCategoryForm,UForm,newsandeventsForm,commentsForm,gettouchwithusForm,UserProfileInfoForm,enquiryForm,usertokensForm,enquiryForm,CategoriesForm,tagsForm,blog_tagsForm,blogsForm
from growelsapp.models import Usercategory,gallerycategory,UserProfileInfo,comments,newsandevents,usertokens,gettouchwithus,enquiry,categories,tags,blog_tags,blogs
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login as dj_login,logout as dj_logout
from django.contrib import messages
from django.template.loader import render_to_string, get_template
from django.utils.html import strip_tags
from django.conf import settings
from django.core.mail import EmailMessage,EmailMultiAlternatives
from django.utils.crypto import get_random_string
from django.contrib.auth.forms import PasswordChangeForm
from django.template import loader 
from django.core.mail import send_mail
from django.conf.global_settings import DEFAULT_FROM_EMAIL
from django.contrib.auth.hashers import make_password
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.core import serializers
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.decorators.csrf import csrf_exempt
import requests



def index(request):
    news =newsandevents.objects.all().order_by('-updated_at').reverse()[:3] 
    return render(request,'growel-feeds/index.html',{'news':news}) 

def about(request): 
    return render(request,'growel-feeds/about-us.html')

def gallery(request): 
    categoryname=Usercategory.objects.filter(status=1).filter(deletestatus=1)
    accountimage = gallerycategory.objects.filter(category__in=Usercategory.objects.filter(status=1).filter(deletestatus=1)).order_by('-updated_at')
    #data = serializers.serialize('json', categoryname)
    #return HttpResponse(data, content_type="application/json")    

    return render(request,'growel-feeds/gallery.html',{'categorynames':categoryname,'accountimages':accountimage}) 


def aqualab(request): 
    return render(request,'growel-feeds/aqua-lab.html')

def blog(request):
    blogss=blogs.objects.filter(category__in=categories.objects.filter(status=1).filter(deletestatus=1)).order_by('-updated_at')
    tag=tags.objects.filter(status=1).filter(deletestatus=1)
    categorynames=categories.objects.filter(status=1).filter(deletestatus=1)
    p = blogs.objects.order_by('title', 'description')
    e=blogs.objects.all().reverse()[:3] 
    if request.method == "POST":  
        forms = gettouchwithusForm(request.POST)
        if forms.is_valid():
            c=gettouchwithus()
            c.first_name=request.POST['first_name']
            c.last_name=request.POST['last_name']
            c.mobile_number=request.POST['mobile_number']
            c.email=request.POST['email']
            c.message=request.POST['message']
            c.save()
            subject = "Greetings"  
            msg     = render_to_string('growelsapp/email-activation.html')
            to      =  c.email
            plain_message = strip_tags(msg)
            with open(settings.BASE_DIR + "/growelsapp/templates/growelsapp/email-activation.txt") as f:
                send_message=f.read()
            message=EmailMultiAlternatives(subject,send_message,settings.EMAIL_HOST_USER, [to]) 
            html_template=get_template("growelsapp/email-activation.html").render()
            message.attach_alternative(html_template,"text/html")
            message.send() 
    return render(request,'growel-feeds/blog.html',{'e':e,'p':p,'blogss':blogss,'tag':tag,'categorynames':categorynames})



def contact(request):

    if request.method == "POST":  
        forms = enquiryForm(request.POST)
        re=commentsForm(request.POST)
        if forms.is_valid() and re.is_valid():
            c=enquiry()
            c.user_name=request.POST['user_name']
            c.user_email=request.POST['user_email']
            c.subject=request.POST['subject']
            c.email_message=request.POST['email_message']
            c.save()
            subject = "Greetings"  
            msg     = render_to_string('growelsapp/email-activation.html')
            to      =  c.user_email
            plain_message = strip_tags(msg)
            with open(settings.BASE_DIR + "/growelsapp/templates/growelsapp/email-activation.txt") as f:
                send_message=f.read()
            message=EmailMultiAlternatives(subject,send_message,settings.EMAIL_HOST_USER, [to]) 
            html_template=get_template("growelsapp/email-activation.html").render()
            message.attach_alternative(html_template,"text/html")
            message.send() 
            recaptcha_response = request.POST.get('g-recaptcha-response')
            data = {
                'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
                'response': recaptcha_response
            }
            r = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
            result = r.json()
            ''' End reCAPTCHA validation '''
            if result['success']:
                re.save()
                messages.success(request, 'New comment added with success!')
            else:
                messages.error(request, 'Invalid reCAPTCHA. Please try again.')    
    return render(request,"growel-feeds/contact.html")

def feeds(request): 
    return render(request,'growel-feeds/feeds.html') 

def fishfeeds(request,slug): 
    return render(request,'growel-feeds/feeds.html') 


def growelsstory(request): 
    return render(request,'growel-feeds/growel-story.html') 

def landing(request): 
    return render(request,'growel-feeds/landing.html')

def managementteam(request): 
    return render(request,'growel-feeds/management-team.html')

def media(request):
    news =newsandevents.objects.filter(status=1).order_by('-updated_at')  
    return render(request,'growel-feeds/media.html',{'news':news})

def mediadetails(request): 
    return render(request,'growel-feeds/mediadetail.html')

def processor(request): 
    return render(request,'growel-feeds/processor.html')

def product(request): 
    return render(request,'growel-feeds/product.html')

def quality(request): 
    return render(request,'growel-feeds/quality.html')    

def randd(request): 
    return render(request,'growel-feeds/r&d.html')

def technology(request): 
    return render(request,'growel-feeds/technology.html')

def login(request):
    if request.session.get('username',None) is not None:
        return redirect('/dashboardpage')
    enquirycount = enquiry.objects.all().count()
    gettouchwithuscount=gettouchwithus.objects.all().count()    
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            request.session['username']=username
            print(username)
            if user.is_active:
                dj_login(request,user)
                return render(request, 'growelsapp/dashboardpage.html',{'enquirycount':enquirycount,'gettouchwithuscount':gettouchwithuscount})
            else:
                return HttpResponse("Your account was inactive.")
        else:
            print("Someone tried to login and failed.")
            print("They used username: {} and password: {}".format(username,password))
            messages.info(request, 'Enter valid username and password!')
            return render(request, 'growelsapp/login.html')
    else:
        return render(request, 'growelsapp/login.html', {'enquirycount':enquirycount,'gettouchwithuscount':gettouchwithuscount})


def logout(request):
    dj_logout(request)
    del request.session
    return redirect("/admin")


def register(request):
    registered = False
    if request.method == 'POST':
        user_form = UForm(data=request.POST)
        profile_form = UserProfileInfoForm(data=request.POST)
        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            profile = profile_form.save(commit=False)
            profile.user = user
            profile.first_name=request.POST['first_name']
            profile.last_name=request.POST['last_name']
            profile.save()
            registered = True
        else:
            print(user_form.errors,profile_form.errors)
            
    else:
        user_form = UForm()
        profile_form = UserProfileInfoForm()
    return render(request,'growelsapp/register.html',
                          {'user_form':user_form,
                           'profile_form':profile_form,
                           'registered':registered})

#LOGIN PAGE    
def resetpasswordconfirm(request,tokens):
        if request.method =='POST':
            new_password=request.POST['new_password2']
            #c=make_password(new_password)
            c=usertokens.objects.filter(tokens=tokens)
            for email in c:
                user_id = email.user_id
            if c:
                m=User.objects.get(id=user_id)    
                m.password=make_password(new_password)
                m.save()
                return render(request, "registrations/password_reset_complete.html",{'c':c}) 
            else:
                return HttpResponse("sorry invalid url") 
        return render(request, "registrations/password_reset_confirm.html")

#RESET PASSWORD 
def resetpassword(request):
    if request.method == "POST":  
        email=request.POST['email']
        unique_id = get_random_string(length=20)
        if User.objects.filter(email=email).exists():
            c=User.objects.filter(email=email)
            user = User.objects.get(email=email)
            if user is not None:
                        email_template_data = {
                    'email': user.email,
                    'domain': request.META['HTTP_HOST'],
                    'tokens': unique_id,
                    'protocol': 'http',
                }
            if c:
                for user_id in c:
                    user_id=user_id.pk
                    if user_id:
                        print(user_id)
                        if usertokens.objects.filter(user_id=user_id).exists():
                            m=usertokens.objects.get(user_id=user_id)    
                            m.tokens=unique_id
                            m.save()    
                        else:
                            active=usertokens(user_id=user_id,tokens=unique_id)
                            active.save()       
                    email_template_name = 'registrations/password_reset_email.html'
                    subject = 'Password reset'
                    email = loader.render_to_string(email_template_name, email_template_data)

                    send_mail(subject, email, DEFAULT_FROM_EMAIL, [user.email], fail_silently=False)
                    return render(request, 'registrations/password_reset_done.html')
        else:
            messages.info(request, 'you entered wrong email address!')
            return render(request, 'registrations/password_reset_form.html')            
    return render(request, 'registrations/password_reset_form.html')

def resetpasswordcomplete(request,tokens):
    delete=usertokens.objects.get(tokens=tokens)
    delete.delete()
    return redirect("/reset-password/success")

def resetpasswordsuccess(request):
    return render(request, 'registrations/password_reset_success.html')

@login_required 
def change_password(request):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            #messages.success(request, 'Your password was successfully updated!')
            return redirect('/dashboardpage')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'growelsapp/passwordchange.html', {
        'form': form
    })    

def dash_board_pages(request):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    enquirycount = enquiry.objects.all().count() 
    gettouchwithuscount=gettouchwithus.objects.all().count() 
    return render(request,'growelsapp/dashboardpage.html',{'enquirycount':enquirycount,'gettouchwithuscount':gettouchwithuscount})    

def enquirylist(request):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    enquiries = enquiry.objects.all().order_by('-updated_at')  
    return render(request,"growelsapp/enquirylist.html",{'enquiries':enquiries})

#ENQUIRY DELETE 
def enquirydelete(request, id):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    Account = enquiry.objects.get(id=id)  
    Account.delete()  
    return redirect("/enquirylist")

def blogcategories(request):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    if request.method == "POST":  
        forms = CategoriesForm(request.POST)
        if forms.is_valid():
            c=categories()
            c.Name=request.POST['Name']
           #c.slug=request.POST['slug']
            c.meta_title=request.POST['meta_title']
            c.meta_keyword =request.POST['meta_keyword']
            c.meta_description=request.POST['meta_description']           
            c.save()
    else:
        forms = CategoriesForm()   
    return render(request,"growelsapp/blogcategories.html",{'forms': forms})


def bclist(request):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    user = categories.objects.filter(deletestatus=1)  
    return render(request,'growelsapp/blogcategorieslist.html', {'user':user}) 

#BLOG CATEGORIES STATUS DEACTIVE 
def categorieslistdeactive(request,id):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    deactive=categories.objects.get(id=id)
    deactive.status=0
    deactive.save()
    return redirect('/blogcategorieslist')

#BLOG CATEGORIES ACTIVE 
def categorieslistactive(request,id):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    active=categories.objects.get(id=id)
    active.status=1
    active.save()
    return redirect('/blogcategorieslist')

#category edit 
def blogcategoryedit(request, id):
    if request.session.get('username',None) == None:
        return redirect('/admin')  
    employee = categories.objects.get(id=id)  
    return render(request,'growelsapp/blogcategoriesedit.html', {'employee':employee})  

#category upgate
def blogcategoryupdate(request, id):
    if request.session.get('username',None) == None:
        return redirect('/admin') 
    employee = categories.objects.get(id=id)
    if request.method == "POST":  
        form = CategoriesForm(request.POST, instance = employee)  
        if form.is_valid():  
            form.save()  
            return redirect("/blogcategorieslist")
    else:
        form=  CategoriesForm()       
    return render(request, 'growelsapp/blogcategoriesedit.html', {'employee': employee,'form':form})

#BLOG CATEGORIES STATUS ACTIVE 
def blogcategorydestroy(request, id): 
    if request.session.get('username',None) == None:
        return redirect('/admin')
    categorydelete = categories.objects.get(id=id)
    categorydelete.deletestatus = 0  
    categorydelete.save() 
    return redirect("/blogcategorieslist")

#TAG
def tag(request):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    if request.method == "POST":  
        forms = tagsForm(request.POST)
        if forms.is_valid():
            c=tags()
            c.Name=request.POST['Name']
            #c.slug=request.POST['slug']
            c.meta_title=request.POST['meta_title']
            c.meta_keyword=request.POST['meta_keyword']
            c.meta_description=request.POST['meta_description']
            c.save() 
    else:
        forms = tagsForm()
    return render(request,"growelsapp/tags.html",{'forms': forms})

#TAG LIST
def taglist(request):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    user = tags.objects.filter(deletestatus=1)  
    return render(request,'growelsapp/tagslist.html', {'user':user})

#category edit 
def tagsedit(request, id):
    if request.session.get('username',None) == None:
        return redirect('/admin') 
    employee = tags.objects.get(id=id)  
    return render(request,'growelsapp/tagsedit.html', {'employee':employee})  

#tags upgate
def tagsupdate(request, id):
    if request.session.get('username',None) == None:
        return redirect('/admin') 
    employee = tags.objects.get(id=id)
    if request.method == "POST":  
        form = tagsForm(request.POST, instance = employee)  
        if form.is_valid():  
            form.save()  
            return redirect("/tagslist") 
    else:
        form=  tagsForm()        
    return render(request, 'growelsapp/tagsedit.html', {'employee': employee,'form':form})

#tags delete
def tagsdestroy(request, id):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    tagsdelete = tags.objects.get(id=id) 
    tagsdelete.deletestatus = 0  
    tagsdelete.save()  
    return redirect("/tagslist") 



# TAG STATUS DEACTIVE 
def tagdeactive(request,id):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    deactive=tags.objects.get(id=id)
    deactive.status=0
    deactive.save()
    return redirect('/tagslist')

#TAG STAUS ACTIVE 
def tagactive(request,id):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    active=tags.objects.get(id=id)
    active.status=1
    active.save()
    return redirect('/tagslist')
#BLOG TAG
def blog_tag(request):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    blogss=blogs.objects.all()
    tagss=tags.objects.all()
    if request.method == "POST":  
        forms = blog_tagsForm(request.POST) 
        if forms.is_valid(): 
            s=blog_tags()
            blog=request.POST['blog']
            tag=request.POST['tag']
            obj=blog_tags(blog_id=blog,tag_id=tag)
            obj.save()
    return render(request,"growelsapp/blog_tags.html",{'tagss':tagss,'blogss':blogss})

#BLOG TAG LIST
def blogtaglist(request):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    user = blog_tags.objects.all()  
    return render(request,'growelsapp/list.html', {'user':user}) 

@csrf_exempt
@ensure_csrf_cookie
def blogimage(request):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    categorynames=categories.objects.filter(status=1).filter(deletestatus=1)
    tagss=tags.objects.filter(status=1).filter(deletestatus=1)
    if request.method == 'POST':  
        print(request.POST.getlist('tags'))
        forms = blogsForm(data=request.POST)
        blogtags = blog_tagsForm(data=request.POST) 
        if forms.is_valid():
            image = request.FILES['image_file']
            unique_id = get_random_string(length=10)
            image_name = "blog/"+unique_id+".png"
            c=blogs()
            category=request.POST['category']
            c.title=request.POST['title']
            #c.slug=request.POST['slug']
            c.description=request.POST['description']
            c.image_alt=request.POST['image_alt']
            c.image_title=request.POST['image_title']
            c.meta_title=request.POST['meta_title']
            c.meta_keyword=request.POST['meta_keyword']
            c.meta_description =request.POST['meta_description'] 
            image_file=image_name
            o=blogs(category_id=category,title=c.title,description=c.description,image_file=image_file,image_alt=c.image_alt,image_title=c.image_title,meta_title=c.meta_title,meta_keyword=c.meta_keyword,meta_description=c.meta_description)
            o.save()
            blog_id = o.id;  
            print(blog_id)
            try:
                from PIL import Image, ImageOps
            except ImportError:
                import Image
                import ImageOps
            image = Image.open(image)
            imageresize = image.resize((600,600), Image.ANTIALIAS)
            image_url = 'growelsapp/media/' +image_name;
            imageresize.save(image_url,quality=100)
        for tag in request.POST.getlist('tags'): 
            blog_tag=blog_tags(blog_id=blog_id,tag_id=tag)
            blog_tag.save()   
    return render(request,"growelsapp/blog.html",{'categorynames':categorynames,'tagss':tagss}) 
        
#BLOG LIST
def blogslist(request):
    if request.session.get('username',None) == None:
        return redirect('/admin')  
    user = blogs.objects.filter(category__in=categories.objects.filter(status=1).filter(deletestatus=1)).order_by('-updated_at')
    return render(request,'growelsapp/bloglist.html', {'user':user}) 
   
#blog edit 
def blogsedit(request, id):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    employee = blogs.objects.get(id=id)
    categorynames=categories.objects.all() 
    tagss=tags.objects.all() 
    return render(request,'growelsapp/blogedit.html', {'tagss':tagss,'employee':employee,'categorynames':categorynames})  

#blog upgate
def blogsupdate(request, id):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    employee = blogs.objects.get(id=id)
    categorynames=categories.objects.all()
    if request.method == "POST":  
        form = blogsForm(request.POST,request.FILES,instance = employee)  
        if form.is_valid(): 
            if request.FILES:
                    image = request.FILES['image_file']
                    unique_id = get_random_string(length=10)
                    image_name = "blog/"+unique_id+".png"
                    s=blogs()
                    image_file=image_name
                    #active=blogs.objects.get(id=id)
                    employee.image_file=image_name
                    employee.save()
                    try:
                        from PIL import Image, ImageOps
                    except ImportError:
                        import Image
                        import ImageOps
                    image = Image.open(image)
                    imageresize = image.resize((600,600), Image.ANTIALIAS)
                    image_url = 'growelsapp/media/' +image_name;
                    imageresize.save(image_url,quality=100) 
            form.save()  
            return redirect("/bloglist")  
    return render(request, 'growelsapp/bloglist.html', {'employee': employee,'categorynames':categorynames})

# blog delete
def blogsdestroy(request, id): 
    if request.session.get('username',None) == None:
        return redirect('/admin')
    blogsdelete = blogs.objects.get(id=id)  
    blogsdelete.delete()  
    return redirect("/bloglist")


# BLOG STATUS DEACTIVE 
def blogdeactive(request,id):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    deactive=blogs.objects.get(id=id)
    deactive.status=0
    deactive.save()
    return redirect('/bloglist')

#BLOG STAUS ACTIVE 
def blogactive(request,id):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    active=blogs.objects.get(id=id)
    active.status=1
    active.save()
    return redirect('/bloglist')


def blogcategoryonclick(request,slug):
    tag=tags.objects.filter(status=1).filter(deletestatus=1)
    #accountimage = gallerycategory.objects.filter(category__in=Usercategory.objects.filter(status=1).filter(deletestatus=1))
    categorynames=categories.objects.filter(status=1).filter(deletestatus=1)
    p = blogs.objects.order_by('title', 'description').first()
    e=blogs.objects.all().reverse()[:5]
    print(slug)   
    category_datas = categories.objects.filter(slug=slug)

    for category in category_datas:
        category_id = category.pk

    # category_id = category.pk
    #print(category_id)

    blog_datas = blogs.objects.filter(category_id=category_id)
    if request.method == "POST":  
        forms = gettouchwithusForm(request.POST)
        if forms.is_valid():
            c=gettouchwithus()
            c.first_name=request.POST['first_name']
            c.last_name=request.POST['last_name']
            c.mobile_number=request.POST['mobile_number']
            c.email=request.POST['email']
            c.message=request.POST['message']
            c.save()
            subject = "Greetings"  
            msg     = render_to_string('growelsapp/email-activation.html')
            to      =  c.email
            plain_message = strip_tags(msg)
            with open(settings.BASE_DIR + "/growelsapp/templates/growelsapp/email-activation.txt") as f:
                send_message=f.read()
            message=EmailMultiAlternatives(subject,send_message,settings.EMAIL_HOST_USER, [to]) 
            html_template=get_template("growelsapp/email-activation.html").render()
            message.attach_alternative(html_template,"text/html")
            message.send()    
    return render(request, 'growelsapp/blogcategoryonclick.html',{'e':e,'p':p,'blog_datas':blog_datas,'tag':tag,'categorynames':categorynames})



# BLOG READ MORE  
def blogreadmoreonclick(request,slug):
    
    p = blogs.objects.order_by('title', 'description').first()
    e=blogs.objects.all().reverse()[:5]
    categorynames=categories.objects.filter(status=1).filter(deletestatus=1)
    tag=tags.objects.filter(status=1).filter(deletestatus=1)
    #accountimage = gallerycategory.objects.filter(category__in=Usercategory.objects.filter(status=1).filter(deletestatus=1))
    blog=blogs.objects.all()
    print(slug)   
    blog_datas = blogs.objects.filter(slug=slug)

    for blog in blog_datas:
        blog_id = blog.pk

    #data = serializers.serialize('json', blog_datas)
    #return HttpResponse(data, content_type="application/json")    

    # category_id = category.pk
    #print(category_id)

    blog_datas = blogs.objects.filter(id=blog_id) 
    if request.method == "POST":  
        forms = gettouchwithusForm(request.POST)
        if forms.is_valid():
            c=gettouchwithus()
            c.first_name=request.POST['first_name']
            c.last_name=request.POST['last_name']
            c.mobile_number=request.POST['mobile_number']
            c.email=request.POST['email']
            c.message=request.POST['message']
            c.save()
            subject = "Greetings"  
            msg     = render_to_string('growelsapp/email-activation.html')
            to      =  c.email
            plain_message = strip_tags(msg)
            with open(settings.BASE_DIR + "/growelsapp/templates/growelsapp/email-activation.txt") as f:
                send_message=f.read()
            message=EmailMultiAlternatives(subject,send_message,settings.EMAIL_HOST_USER, [to]) 
            html_template=get_template("growelsapp/email-activation.html").render()
            message.attach_alternative(html_template,"text/html")
            message.send()
    return render(request, 'growelsapp/blogreadmoreonclick.html',{'p':p,'e':e,'categorynames':categorynames,'blog_datas':blog_datas,'tag':tag})



# TAGS ON CLICK FROM BLOG PAGE 
def tagsonclick(request,slug):
    p = blogs.objects.order_by('title', 'description').first()
    e=blogs.objects.all().reverse()[:5]
    categorynames=categories.objects.filter(status=1).filter(deletestatus=1)
    tag=tags.objects.filter(status=1).filter(deletestatus=1)
    print(slug)
    #accountimage = gallerycategory.objects.filter(category__in=Usercategory.objects.filter(status=1).filter(deletestatus=1))
    blog=blogs.objects.filter(category__in=categories.objects.filter(status=1).filter(deletestatus=1)) 
    print(slug)  
    tag_datas = tags.objects.filter(slug=slug)

    for tagss in tag_datas:
        tag_id= tagss.pk
    
    blog_datas = blog_tags.objects.filter(tag_id=tag_id)
    
    blog_ids = []
    for blog_data in blog_datas:
        blog_ids.append(blog_data.blog_id)
  

    print(blog_ids)    
    blog_list=blogs.objects.filter(id__in=blog_ids).filter(category__in=categories.objects.filter(status=1).filter(deletestatus=1))   
    if request.method == "POST":  
        forms = gettouchwithusForm(request.POST)
        if forms.is_valid():
            c=gettouchwithus()
            c.first_name=request.POST['first_name']
            c.last_name=request.POST['last_name']
            c.mobile_number=request.POST['mobile_number']
            c.email=request.POST['email']
            c.message=request.POST['message']
            c.save()
            subject = "Greetings"  
            msg     = render_to_string('growelsapp/email-activation.html')
            to      =  c.email
            plain_message = strip_tags(msg)
            with open(settings.BASE_DIR + "/growelsapp/templates/growelsapp/email-activation.txt") as f:
                send_message=f.read()
            message=EmailMultiAlternatives(subject,send_message,settings.EMAIL_HOST_USER, [to]) 
            html_template=get_template("growelsapp/email-activation.html").render()
            message.attach_alternative(html_template,"text/html")
            message.send()
    return render(request, 'growelsapp/blogtagsonclick.html',{'p':p,'e':e,'categorynames':categorynames,'blog_list':blog_list,'tag':tag})


def gettouchwithuslist(request):
    get=gettouchwithus.objects.all()
    return render(request, 'growelsapp/gettouchwithuslist.html',{'get':get})

def gettouchwithusdelete(request,id):
    delete=gettouchwithus.objects.get(id=id)
    delete.delete()
    return redirect("/touchlist")


def news(request): 
    if request.session.get('username',None) == None:
        return redirect('/admin')
    if request.method == "POST":  
        forms = newsandeventsForm(request.POST,request.FILES) 
        if forms.is_valid():
            image = request.FILES['image_file']
            #unique_id = get_random_string(length=10)
            #image_name = "news" +unique_id+".png"
            s=newsandevents()
            s.title=request.POST['title']
            s.image_file=image
            s.save() 
            
    else:
        forms= newsandeventsForm()
    return render(request,'growelsapp/newsandevents.html',{'forms':forms})          
    

def newslist(request):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    homesummeryservicelist = newsandevents.objects.all().order_by('-updated_at')  
    return render(request,"growelsapp/newsandeventslist.html",{'homesummeryservicelist':homesummeryservicelist})

#HOME SERVICE EDIT
def newsedit(request, id):
    if request.session.get('username',None) == None:
        return redirect('/admin')  
    edit = newsandevents.objects.get(id=id)  
    return render(request,'growelsapp/newsandeventsedit.html', {'edit':edit}) 

#HOME SERVICE UPDATE 
def newsupdate(request, id):
    if request.session.get('username',None) == None:
        return redirect('/admin')  
    edit = newsandevents.objects.get(id=id)  
    form = newsandeventsForm(request.POST,request.FILES, instance = edit)  
    if form.is_valid():
        form.save()   
    return redirect("/newsandeventslist")  
    #return render(request, 'growelsapp/newsandeventslist.html', {'update': update})

#HOME SERVICE DELETE     
def newsdestroy(request, id): 
    if request.session.get('username',None) == None:
        return redirect('/admin') 
    Account = newsandevents.objects.get(id=id)  
    Account.delete()  
    return redirect("/newsandeventslist") 

def deactivenewsandeventslist(request,id):
    t = newsandevents.objects.get(id=id)
    form = newsandeventsForm(request.POST,request.FILES, instance = t) 
    t.status = 0  
    t.save() 
    return redirect("/newsandeventslist") 

#HOME SERVICE STATUS ACTIVE
def activenewsandeventslist(request,id):
    t = newsandevents.objects.get(id=id)
    form = newsandeventsForm(request.POST,request.FILES, instance = t)
    t.status = 1  
    t.save() 
    return redirect("/newsandeventslist")  


def category(request):
    if request.session.get('username',None) == None:
        return redirect('/admin')   
    if request.method == "POST":  
        forms = UserCategoryForm(request.POST)
        if forms.is_valid():
            category=Usercategory()
            category.name=request.POST['name']
            category.save()
    else:
        forms = UserCategoryForm()
    return render(request,'growelsapp/category.html',{'forms': forms})

def categorylist(request):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    categorylist = Usercategory.objects.filter(deletestatus=1)
    return render(request,"growelsapp/categorylist.html",{'categorylist':categorylist}) 




#category delete     
def categorydestroy(request, id): 
    if request.session.get('username',None) == None:
        return redirect('/admin') 
    categorydelete = Usercategory.objects.get(id=id)
    categorydelete.deletestatus = 0  
    categorydelete.save() 
    return redirect("/categorylist")
    

#category edit 
def categoryedit(request, id): 
    if request.session.get('username',None) == None:
        return redirect('/admin') 
    employee = Usercategory.objects.get(id=id)  
    return render(request,'growelsapp/categoryedit.html', {'employee':employee})  



#category upgate
def categoryupdate(request, id):
    if request.session.get('username',None) == None:
        return redirect('/admin')  
    employee = Usercategory.objects.get(id=id)
    if request.method == "POST":  
        form = UserCategoryForm(request.POST, instance = employee)  
        if form.is_valid():  
            form.save()
            messages.info(request, 'Updated successfully!')  
            return redirect("/categorylist") 
    else:
        form= UserCategoryForm()    
    return render(request, 'growelsapp/categoryedit.html', {'employee': employee,'form':form})

#DEACTIVE STATUS
def deactivecategorylist(request,id):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    categorydeactive=Usercategory.objects.get(id=id)
    categorydeactive.status=0
    categorydeactive.save()
    #messages.info(request, 'Record deactive successfully!')
    return redirect('/categorylist')


#ACTIVE STATUS
def activecategorylist(request,id):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    active=Usercategory.objects.get(id=id)
    active.status=1
    active.save()
    #messages.info(request, 'Record active successfully!')
    return redirect('/categorylist')



def gallerycategories(request):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    categoryname=Usercategory.objects.filter(status=1)
    if request.method == "POST":  
        forms = galleryCategoryForm(request.POST,request.FILES) 
        if forms.is_valid(): 
            image = request.FILES['image_file']
            unique_id = get_random_string(length=10)
            image_name = "gallery/"+unique_id+".png"
            s=gallerycategory()
            category=request.POST['category']
            image_file=image_name
            obj=gallerycategory(image_file=image,category_id=category)
            obj.save()

    else:
        forms= galleryCategoryForm()    
    return render(request,'growelsapp/categoryimage.html',{'categoryname':categoryname,'forms':forms})    

def categoryimagelist(request):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    user = gallerycategory.objects.all().order_by('-updated_at')  
    return render(request,"growelsapp/categoryimagelist.html",{'user':user})    


#CATEGORY DELETE
def categoryimagedelete(request, id):
    if request.session.get('username',None) == None:
        return redirect('/admin')  
    user = gallerycategory.objects.get(id=id)  
    user.delete()  
    return redirect("/categoryimagelist")


#category edit 
def categoryimageedit(request, id): 
    if request.session.get('username',None) == None:
        return redirect('/admin') 
    galleryedit = gallerycategory.objects.get(id=id)
    categoryname=Usercategory.objects.all()              
    return render(request,'growelsapp/categoryimageedit.html', {'galleryedit':galleryedit,'categoryname':categoryname})  

#category upDate
'''
def categoryimageupdate(request, id):
    if request.session.get('username',None) == None:
        return redirect('/admin_login')  
    gallery = gallerycategory.objects.get(id=id)
    if request.method == 'POST':  
        forms = galleryCategoryForm(request.POST,request.FILES, instance = gallery)  
        if forms.is_valid():
            forms.save()   
            return redirect("/categoryimagelist")
    else:
        forms = galleryCategoryForm() 
    return render(request, 'temp/categoryimagelist.html',{'forms':forms})
'''
def categoryimageupdate(request, id):
    if request.session.get('username',None) == None:
        return redirect('/admin')  
    galleryedit = gallerycategory.objects.get(id=id)
    categoryname=Usercategory.objects.all() 
    if request.method == "POST":  
        forms = galleryCategoryForm(request.POST, request.FILES,instance = galleryedit)  
        if forms.is_valid():  
            forms.save()
            return redirect("/categoryimagelist") 
    else:
        forms= galleryCategoryForm()
    return render(request, 'growelsapp/categoryimageedit.html', {'categoryname':categoryname,'galleryedit': galleryedit,'forms':forms})
