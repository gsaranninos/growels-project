from django.apps import AppConfig


class GrowelsappConfig(AppConfig):
    name = 'growelsapp'
